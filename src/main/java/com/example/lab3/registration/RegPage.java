package com.example.lab3.registration;

import com.example.lab3.Page;
import org.openqa.selenium.WebDriver;

public class RegPage extends Page {

    private final String buyServerButton = "//div[2]/div[3]/a";
    private final String regButton = "/html/body/div[4]/div/div/div[17]/form/div[2]/div[2]/div[12]";

    private final String regName = "//div/div[3]/div[2]/input";
    private final String regEmail = "/html/body/div[4]/div/div/div[17]/form/div[2]/div[2]/div[4]/div/div/div[2]/input";
    private final String regPhone = "/html/body/div[4]/div/div/div[17]/form/div[2]/div[2]/div[5]/div/div[2]/input";
    private final String regBadCheck = "/html/body/div[4]/div/div/div[17]/form/div[2]/div[2]/div[4]/div/div/div[2]/input";

    public RegPage(WebDriver driver) {
        super(driver);
    }

    public String getBuyServerButton() {
        return buyServerButton;
    }

    public String getRegBadCheck() {
        return regBadCheck;
    }

    public String getRegButton() {
        return regButton;
    }

    public String getRegEmail() {
        return regEmail;
    }

    public String getRegName() {
        return regName;
    }

    public String getRegPhone() {
        return regPhone;
    }
}
