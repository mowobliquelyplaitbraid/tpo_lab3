package com.example.lab3.pages;

import com.example.lab3.Page;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class DomainPage extends Page {

    private final String domainPage = "//li[4]/a/span/span";
    private final String domainMovePage = "//a[3]";
    private final String domainWhoisPage = "//li[4]/div/a[2]";

    private final String whoisSearch = "//input[@id='domain-search-input']";
    private final String whoisSearchButton = "//div[2]/div/div/div[2]/a";
    private final String whoisResult = "/html/body/div[2]/div[3]/div[3]/div/div/pre";

    private final String moveName = "//label/input";
    private final String moveEmail = "//div[2]/label/input";
    private final String moveText = "//textarea";
    private final String moveButton = "//form/div[2]/div";
    private final String moveResult = "//div[4]/div/div/div[17]/form/div[2]/div";

    public DomainPage(WebDriver driver) {
        super(driver);
    }

    public void checkDomainMenu() {
        WebElement element = driver.findElement(By.xpath(domainPage));
        Actions builder = new Actions(driver);
        builder.moveToElement(element).perform();
    }

    public void changeToDomainWhoisPage() {
        this.checkDomainMenu();
        driver.findElement(By.xpath(domainWhoisPage)).click();
        (new WebDriverWait(driver, 30)).until(ExpectedConditions.titleIs("Whois сервис, проверить домен на занятость, информация о сайте"));
    }

    public void changeToDomainMovePage() {
        this.checkDomainMenu();
        driver.findElement(By.xpath(domainMovePage)).click();
        (new WebDriverWait(driver, 30)).until(ExpectedConditions.titleIs("Перенос домена, перенести домен в Timeweb"));
    }

    public String getDomainMovePage() {
        return domainMovePage;
    }

    public String getDomainPage() {
        return domainPage;
    }

    public String getDomainWhoisPage() {
        return domainWhoisPage;
    }

    public String getMoveButton() {
        return moveButton;
    }

    public String getMoveEmail() {
        return moveEmail;
    }

    public String getMoveName() {
        return moveName;
    }

    public String getMoveResult() {
        return moveResult;
    }

    public String getMoveText() {
        return moveText;
    }

    public String getWhoisResult() {
        return whoisResult;
    }

    public String getWhoisSearch() {
        return whoisSearch;
    }

    public String getWhoisSearchButton() {
        return whoisSearchButton;
    }
}
