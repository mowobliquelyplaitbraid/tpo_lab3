package com.example.lab3.pages;

import com.example.lab3.Page;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class VpsPage extends Page {

    private final String vpsPage = "//li[2]/a/span/span";

    private final String vpsConfigurationButton = "//div[2]/div[2]/div/div[2]/div";
    private final String osConfigurationChange = "//div[3]/div/div[2]/div/div/div/div[2]/div[2]";
    private final String finalPrice = "//*[@id=\"vds-configurator-form\"]/div[4]/span[2]";
    private final String DDoSConfiguration = "//div[2]/div/div/div/input";
    private final String ispConfiguration = "//*[@id=\"vds-configurator\"]/div[3]/div[2]/div[2]/div[1]/div[2]/div/div";

    public VpsPage(WebDriver driver) {
        super(driver);
    }

    public void changeToVpsPage() {
        driver.findElement(By.xpath(vpsPage)).click();
        (new WebDriverWait(driver, 30)).until(ExpectedConditions.titleIs("Аренда VDS/VPS сервера, купить виртуальный сервер от Timeweb"));
    }

    public String getDDoSConfiguration() {
        return DDoSConfiguration;
    }

    public String getFinalPrice() {
        return finalPrice;
    }

    public String getIspConfiguration() {
        return ispConfiguration;
    }

    public String getOsConfigurationChange() {
        return osConfigurationChange;
    }

    public String getVpsConfigurationButton() {
        return vpsConfigurationButton;
    }

    public String getVpsPage() {
        return vpsPage;
    }
}
