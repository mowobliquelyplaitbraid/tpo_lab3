package com.example.lab3.pages;

import com.example.lab3.Page;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class DedicatedPage extends Page {

    private final String dedicatedPage = "//li[5]/a/span";

    private final String dedicatedConfigurationButton = "//div[2]/div[3]/div[3]/div/div[2]/div[2]/div";
    private final String dedicatedBuyingButton = "//div[2]/div/button";
    private final String dedicatedConfigurationCheck = "/html/body/div[4]/div/div/div[19]/form/div[2]/div[2]/div[1]/div[1]/span[2]";

    public DedicatedPage(WebDriver driver) {
        super(driver);
    }

    public void changeToDedicatedPage() {
        driver.findElement(By.xpath(dedicatedPage)).click();
        (new WebDriverWait(driver, 30)).until(ExpectedConditions.titleIs("Аренда выделенного сервера (dedicated server) | Купить физический сервер в России, СНГ"));
    }

    public String getDedicatedBuyingButton() {
        return dedicatedBuyingButton;
    }

    public String getDedicatedConfigurationButton() {
        return dedicatedConfigurationButton;
    }

    public String getDedicatedConfigurationCheck() {
        return dedicatedConfigurationCheck;
    }

    public String getDedicatedPage() {
        return dedicatedPage;
    }
}
