package com.example.lab3;

import org.openqa.selenium.WebDriver;

public class Page {
    protected WebDriver driver = null;
    private String title;

    public Page(WebDriver driver) {
        this.driver = driver;
        title = driver.getTitle();
    }

    public String getTitle() {
        return driver.getTitle();
    }
}
