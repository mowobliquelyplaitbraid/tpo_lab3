package org.example.lab3;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class MainPage {

    public MainPage(WebDriver driver) {
        this.driver = driver;
        this.title = driver.getTitle();
    }

    WebDriver driver;

    private String title;

    private String domainPage = "//li[4]/a/span/span";

    private String domainMovePage = "//a[3]";

    private String domainWhoisPage = "//li[4]/div/a[2]";

    private String dedicatedPage = "//li[5]/a/span";

    private String vpsPage = "//li[2]/a/span/span";

    public void checkDomainMenu() {
        WebElement element = driver.findElement(By.xpath(domainPage));
        Actions builder = new Actions(driver);
        builder.moveToElement(element).perform();
    }

    public void changeToDomainWhoisPage() {
        this.checkDomainMenu();
        driver.findElement(By.xpath(domainWhoisPage)).click();
    }

    public void changeToDomainMovePage() {
        this.checkDomainMenu();
        driver.findElement(By.xpath(domainMovePage)).click();
    }

    public void changeToDedicatedPage() {
        driver.findElement(By.xpath(dedicatedPage)).click();
    }

    public void changeToVpsPage() {
        driver.findElement(By.xpath(vpsPage)).click();
    }

    public String getTitle() {
        return driver.getTitle();
    }
}
