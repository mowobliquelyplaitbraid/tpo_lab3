package com.example.lab3.registration;

import com.example.lab3.Utils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class RegTest {

    @BeforeEach
    public void setUp() {
        Utils.prepareDrivers();
    }

    @Test
    public void regBad() {
        List<WebDriver> drivers = Utils.getDrivers();
        drivers.parallelStream().forEach(driver -> {
            driver.get(Utils.BASE_URL);
            driver.manage().window().maximize();
            RegPage regPage = new RegPage(driver);

            driver.findElement(By.xpath(regPage.getBuyServerButton())).click();
            driver.findElement(By.xpath(regPage.getRegButton())).click();

            Assertions.assertEquals("suggestions-input error", driver.findElement(By.xpath(regPage.getRegBadCheck())).getAttribute("class"));

            driver.quit();
        });
    }

    @Test
    public void regGood() {
        List<WebDriver> drivers = Utils.getDrivers();
        drivers.parallelStream().forEach(driver -> {
            driver.get(Utils.BASE_URL);
            driver.manage().window().maximize();
            RegPage regPage = new RegPage(driver);

            driver.findElement(By.xpath(regPage.getBuyServerButton())).click();
            driver.findElement(By.xpath(regPage.getRegName())).click();
            driver.findElement(By.xpath(regPage.getRegName())).sendKeys("test");
            driver.findElement(By.xpath(regPage.getRegEmail())).click();
            driver.findElement(By.xpath(regPage.getRegEmail())).sendKeys("test@mail.ru");
            driver.findElement(By.xpath(regPage.getRegPhone())).click();
            driver.findElement(By.xpath(regPage.getRegPhone())).sendKeys("9999999999");
            driver.findElement(By.xpath(regPage.getRegButton())).click();

            (new WebDriverWait(driver, 30)).until(ExpectedConditions.titleIs("Хостинг Timeweb: Панель управления"));
            Assertions.assertEquals("Хостинг Timeweb: Панель управления", driver.getTitle());

            driver.quit();
        });
    }
}
