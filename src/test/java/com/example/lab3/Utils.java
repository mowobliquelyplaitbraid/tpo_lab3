package com.example.lab3;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class Utils {

    public static final String CHROME_SYSTEM_PROPERTY_NAME = "webdriver.chrome.driver";
    public static final String CHROME_SYSTEM_PROPERTY_PATH = "chromedriver";
    public static final String FIREFOX_SYSTEM_PROPERTY_NAME = "webdriver.gecko.driver";
    public static final String FIREFOX_SYSTEM_PROPERTY_PATH = "geckodriver";
    public static final String BASE_URL = "https://timeweb.com/";


    public static List<WebDriver> getDrivers() {
        List<WebDriver> drivers = new ArrayList<>();
        try {
            List<String> properties = Files.readAllLines(Paths.get("site.properties"));
            for (String property : properties) {
                if (property.startsWith("WEB_DRIVER")) {
                    switch (property.toLowerCase().split("=")[1]) {
                        case "chrome":
                            drivers.add(getChromeDriver());
                            return drivers;
                        case "firefox":
                            drivers.add(getFirefoxDriver());
                            return drivers;
                        case "both":
                            drivers.add(getChromeDriver());
                            drivers.add(getFirefoxDriver());
                            return drivers;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        throw new RuntimeException("Web driver is not specified");
    }

    public static List<WebDriver> getDriversPrivate() {
        List<WebDriver> drivers = new ArrayList<>();
        try {
            List<String> properties = Files.readAllLines(Paths.get("site.properties"));
            for (String property : properties) {
                if (property.startsWith("WEB_DRIVER")) {
                    switch (property.toLowerCase().split("=")[1]) {
                        case "chrome":
                            drivers.add(getChromeDriver());
                            return drivers;
                        case "firefox":
                            drivers.add(getFirefoxDriver());
                            return drivers;
                        case "both":
                            drivers.add(getChromeDriver());
                            drivers.add(getFirefoxDriver());
                            return drivers;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        throw new RuntimeException("Web driver is not specified");
    }

    private static ChromeDriver getChromeDriver() {
        if (!System.getProperties().containsKey(CHROME_SYSTEM_PROPERTY_NAME)) {
            throw new RuntimeException("Chrome driver isn't set properly");
        }
        DesiredCapabilities caps = new DesiredCapabilities();

        ChromeOptions options = new ChromeOptions();
        options.addArguments("--incognito");
        caps.setCapability(ChromeOptions.CAPABILITY,options);

        return new ChromeDriver();
    }

    private static FirefoxDriver getFirefoxDriver() {
        if (!System.getProperties().containsKey(FIREFOX_SYSTEM_PROPERTY_NAME)) {
            throw new RuntimeException("Firefox driver isn't set properly");
        }
        DesiredCapabilities caps = new DesiredCapabilities();

        FirefoxOptions options = new FirefoxOptions();
        options.addArguments("-private");
        caps.setCapability("moz:firefoxOptions",options);

        return new FirefoxDriver();
    }

    private static ChromeDriver getChromeDriverPrivate() {
        if (!System.getProperties().containsKey(CHROME_SYSTEM_PROPERTY_NAME)) {
            throw new RuntimeException("Chrome driver isn't set properly");
        }
        DesiredCapabilities caps = new DesiredCapabilities();

        ChromeOptions options = new ChromeOptions();
        options.addArguments("--incognito");
        caps.setCapability(ChromeOptions.CAPABILITY,options);

        return new ChromeDriver(options);
    }

    private static FirefoxDriver getFirefoxDriverPrivate() {
        if (!System.getProperties().containsKey(FIREFOX_SYSTEM_PROPERTY_NAME)) {
            throw new RuntimeException("Firefox driver isn't set properly");
        }
        DesiredCapabilities caps = new DesiredCapabilities();

        FirefoxOptions options = new FirefoxOptions();
        options.addArguments("-private");
        caps.setCapability("moz:firefoxOptions",options);

        return new FirefoxDriver(options);
    }

    public static void prepareDrivers() {
        System.setProperty(CHROME_SYSTEM_PROPERTY_NAME, CHROME_SYSTEM_PROPERTY_PATH);
        System.setProperty(FIREFOX_SYSTEM_PROPERTY_NAME, FIREFOX_SYSTEM_PROPERTY_PATH);
    }
}

