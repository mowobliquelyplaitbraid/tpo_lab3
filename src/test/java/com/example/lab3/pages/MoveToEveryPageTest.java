package com.example.lab3.pages;

import com.example.lab3.Utils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;

import java.util.List;

public class MoveToEveryPageTest {

    @BeforeEach
    public void setUp() {
        Utils.prepareDrivers();
    }

    @Test
    public void MoveToDedicatedPageTest() {
        List<WebDriver> drivers = Utils.getDrivers();
        drivers.parallelStream().forEach(driver -> {
            driver.get(Utils.BASE_URL);
            driver.manage().window().maximize();
            DedicatedPage dedicatedPage = new DedicatedPage(driver);
            String title;

            dedicatedPage.changeToDedicatedPage();
            title = dedicatedPage.getTitle();

            Assertions.assertEquals("Аренда выделенного сервера (dedicated server) | Купить физический сервер в России, СНГ", title);

            driver.quit();
        });
    }


    @Test
    public void MoveToDomainPageTest() {
        List<WebDriver> drivers = Utils.getDrivers();
        drivers.parallelStream().forEach(driver -> {
            driver.get(Utils.BASE_URL);
            driver.manage().window().maximize();
            DomainPage domainPage = new DomainPage(driver);
            String title;

            domainPage.changeToDomainWhoisPage();
            title = domainPage.getTitle();

            Assertions.assertEquals("Whois сервис, проверить домен на занятость, информация о сайте", title);

            domainPage.changeToDomainMovePage();
            title = domainPage.getTitle();

            Assertions.assertEquals("Перенос домена, перенести домен в Timeweb", title);

            driver.quit();
        });
    }


    @Test
    public void MoveToVpsPageTest() {
        List<WebDriver> drivers = Utils.getDrivers();
        drivers.parallelStream().forEach(driver -> {
            driver.get(Utils.BASE_URL);
            driver.manage().window().maximize();
            VpsPage vpsPage = new VpsPage(driver);
            String title;

            vpsPage.changeToVpsPage();
            title = vpsPage.getTitle();
            Assertions.assertEquals("Аренда VDS/VPS сервера, купить виртуальный сервер от Timeweb", title);

            driver.quit();
        });
    }

}
