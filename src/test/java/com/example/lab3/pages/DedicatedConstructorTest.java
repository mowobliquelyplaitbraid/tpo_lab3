package com.example.lab3.pages;

import com.example.lab3.Utils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class DedicatedConstructorTest {

    @BeforeEach
    public void setUp() {
        Utils.prepareDrivers();
    }


    @Test
    public void dedicated() {
        List<WebDriver> drivers = Utils.getDrivers();
        drivers.parallelStream().forEach(driver -> {
            driver.get(Utils.BASE_URL);
            driver.manage().window().maximize();
            DedicatedPage dedicatedPage = new DedicatedPage(driver);
            dedicatedPage.changeToDedicatedPage();

            (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.xpath(dedicatedPage.getDedicatedConfigurationButton())));
            driver.findElement(By.xpath(dedicatedPage.getDedicatedConfigurationButton())).click();
            driver.findElement(By.xpath("//div[2]/span")).click();
            driver.findElement(By.xpath("//form/div[2]/div/div/div[2]/ul/li[4]")).click();
            (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.xpath(dedicatedPage.getDedicatedBuyingButton())));
            driver.findElement(By.xpath(dedicatedPage.getDedicatedBuyingButton())).click();

            (new WebDriverWait(driver, 10)).until(ExpectedConditions.textToBe(By.xpath(dedicatedPage.getDedicatedConfigurationCheck()), "2 х Intel Xeon E5-2620v4 (12 ядер x 2.4-3.2 ГГц)"));
            Assertions.assertEquals("2 х Intel Xeon E5-2620v4 (12 ядер x 2.4-3.2 ГГц)", driver.findElement(By.xpath(dedicatedPage.getDedicatedConfigurationCheck())).getText());

            driver.quit();
        });
    }
}
