package com.example.lab3.pages;

import com.example.lab3.Utils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.util.List;

public class DomainInfoTest {

    @BeforeEach
    public void setUp() {
        Utils.prepareDrivers();
    }

    @Test
    public void domainInfo() {
        List<WebDriver> drivers = Utils.getDrivers();
        drivers.parallelStream().forEach(driver -> {
            driver.get(Utils.BASE_URL);
            driver.manage().window().maximize();
            DomainPage domainPage = new DomainPage(driver);
            domainPage.changeToDomainWhoisPage();

            driver.findElement(By.xpath(domainPage.getWhoisSearch())).click();
            driver.findElement(By.xpath(domainPage.getWhoisSearch())).sendKeys("test.ru");
            driver.findElement(By.xpath(domainPage.getWhoisSearchButton())).click();

            Assertions.assertEquals(true, driver.findElement(By.xpath(domainPage.getWhoisResult())).getText().contains("TEST.RU"));

            driver.quit();
        });
    }
}
