package com.example.lab3.pages;

import com.example.lab3.Utils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

import javax.crypto.spec.PSource;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class VpsConstructorTest {

    @BeforeEach
    public void setUp() {
        Utils.prepareDrivers();
    }

    @Test
    public void vps() {
        List<WebDriver> drivers = Utils.getDrivers();
        drivers.parallelStream().forEach(driver -> {
            driver.get(Utils.BASE_URL);
            driver.manage().window().maximize();
            VpsPage vpsPage = new VpsPage(driver);
            vpsPage.changeToVpsPage();

            driver.findElement(By.xpath(vpsPage.getVpsConfigurationButton())).click();
            driver.findElement(By.xpath(vpsPage.getDDoSConfiguration())).click();
            try {
                Thread.sleep(3*1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            driver.findElement(By.xpath(vpsPage.getOsConfigurationChange())).click();
            (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.xpath(vpsPage.getIspConfiguration())));
            driver.findElement(By.xpath(vpsPage.getIspConfiguration())).click();

            (new WebDriverWait(driver, 10)).until(ExpectedConditions.textToBe(By.xpath(vpsPage.getFinalPrice()), "2586 руб."));
            Assertions.assertEquals("2586 руб.", driver.findElement(By.xpath(vpsPage.getFinalPrice())).getText());

            driver.quit();
        });
    }
}
