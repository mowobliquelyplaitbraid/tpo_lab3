package com.example.lab3.pages;

import com.example.lab3.Utils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class DomainMoveTest {

    @BeforeEach
    public void setUp() {
        Utils.prepareDrivers();
    }

    @Test
    public void domainMove() {
        List<WebDriver> drivers = Utils.getDrivers();
        drivers.parallelStream().forEach(driver -> {
            driver.get(Utils.BASE_URL);
            driver.manage().window().maximize();
            DomainPage domainPage = new DomainPage(driver);
            domainPage.changeToDomainMovePage();

            driver.findElement(By.xpath(domainPage.getMoveName())).click();
            driver.findElement(By.xpath(domainPage.getMoveName())).sendKeys("Test");
            driver.findElement(By.xpath(domainPage.getMoveEmail())).click();
            driver.findElement(By.xpath(domainPage.getMoveEmail())).sendKeys("test@yandex.ru");
            driver.findElement(By.xpath(domainPage.getMoveText())).click();
            driver.findElement(By.xpath(domainPage.getMoveText())).sendKeys("Test");
            driver.findElement(By.xpath(domainPage.getMoveButton())).click();
            (new WebDriverWait(driver, 10)).until(ExpectedConditions.textToBe(By.xpath(domainPage.getMoveResult()), "Спасибо за вашу заявку"));

            Assertions.assertEquals("Спасибо за вашу заявку", driver.findElement(By.xpath(domainPage.getMoveResult())).getText());

            driver.quit();
        });
    }
}